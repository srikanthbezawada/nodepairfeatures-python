# https://stackoverflow.com/questions/29838746/how-to-draw-subgraph-using-networkx
# https://stackoverflow.com/questions/16150557/networkxcreating-a-subgraph-induced-from-edges
# https://stackoverflow.com/questions/16910538/efficient-extraction-of-a-subgraph-according-to-some-edge-attribute-in-networkx

import networkx as nx
import random
import math

# https://stackoverflow.com/questions/22121475/how-to-choose-edges-randomly-in-my-graph
def get_rand_edges(g, n):
    visited = set()
    results = []
    edges = random.sample(g.edges(), n)
    for edge in edges:
        results.append(edge)
        if len(results) == n:
            break

    return results


    
def main():
    TABD = "\t"
    CSVD = ","
    SPACED = " "
    newld = "\n"


    writedelim = CSVD
    readdelim = TABD


    filename = "test.txt"


    G = nx.read_edgelist(filename, delimiter=readdelim)

    print("Printing input graph ~~~~~")
    print(nx.info(G))
    print(G.nodes())
    print(G.edges())




    print("~~~ Splitting ...")
    subedgesSize = int(len(G.edges()) * 0.70)
    randEdges = get_rand_edges(G, subedgesSize)
    S = nx.Graph(randEdges)

    print("Printing split graph ~~~~~")
    print(nx.info(S))
    print(S.nodes())
    print(S.edges())



    G.remove_edges_from(randEdges)


    isolatedNodes = []
    for node in G.nodes():
        if G.degree(node) == 0:
            isolatedNodes.append(node)

    G.remove_nodes_from(isolatedNodes)
    print("modified input graph ~~~~")
    print(nx.info(G))
    print(G.nodes())
    print(G.edges())


    nx.write_edgelist(G,'testEdgeList.csv',data=False, delimiter = ",")
    nx.write_edgelist(S,'trainEdgeList.csv',data=False, delimiter = ",")



    
main()