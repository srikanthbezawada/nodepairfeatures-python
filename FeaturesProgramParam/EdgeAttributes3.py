import networkx as nx
import math
import numpy as np
import sys
import random
import argparse
from numpy import median
#Networkx, Scipy, Numpy


def getRAIndex(G, node1, node2):
    return nx.resource_allocation_index(G, [(node1, node2)])


def getJaccardIndex(G, node1, node2):
    return nx.jaccard_coefficient(G, [(node1, node2)])


def getAtomicAdarIndex(G, node1, node2):
    return nx.adamic_adar_index(G, [(node1, node2)])


def getPrefAtt(G, node1, node2):
    return nx.preferential_attachment(G, [(node1, node2)])


def getShortestPath(G, node1, node2):
    return nx.shortest_path(G, source=node1, target=node2)


def getNumCommonNeighbour(G, node1, node2):
    return len(list(nx.common_neighbors(G, node1, node2)))


def getNumUnionNeighbour(G, node1, node2):
    return len(G.neighbors(node1)) + len(G.neighbors(node2)) - getNumCommonNeighbour(G, node1, node2)


def getCosine(G, node1, node2):
    if len(G.neighbors(node1)) == 0:
        return sys.float_info.max

    if len(G.neighbors(node2)) == 0:
        return sys.float_info.max

    return getNumCommonNeighbour(G, node1, node2)/float(len(G.neighbors(node1)) * len(G.neighbors(node2)))



def getPowerLawExponent(G, node1, node2):
    unionnum = getNumUnionNeighbour(G, node1, node2)

    if unionnum == 0:
        ple = 0
    elif unionnum == 1:
        ple = sys.float_info.max
    else:
        ple = math.log10(G.number_of_edges()) / math.log10(unionnum)

    return ple



def getMaximumFlow(G, node1, node2):
    nx.set_edge_attributes(G, 'capacity', dict(list(zip(G.edges(), [1.0 for edge in G.edges()]))))
    flow_value, flow_dict = nx.maximum_flow(G, node1, node2)
    return flow_value



def getEdgeBetweenness(G, node1, node2):
    if G.has_edge(node1, node2):
        mydict = nx.edge_betweenness_centrality(G, True)
        edgebetweenness = mydict.get((node1, node2), mydict.get((node2, node1)))
        # https://groups.google.com/forum/#!topic/networkx-discuss/m1GBuzDBCnI
    else:
        H = nx.Graph(G)
        H.add_edge(node1, node2)
        mydict = nx.edge_betweenness_centrality(H, True)
        edgebetweenness = mydict.get((node1, node2), mydict.get((node2, node1)))
        H.remove_edge(node1, node2)

    return edgebetweenness

    """
    if G.has_edge(node1, node2):
        mydict = nx.edge_betweenness_centrality(G, True)
        edgebetweenness = mydict.get((node1, node2))

        if edgebetweenness is None:
            edgebetweenness = mydict.get((node2, node1))


    else:
        G.add_edge(node1, node2)
        mydict = nx.edge_betweenness_centrality(G, True)
        e = (node1, node2)
        edgebetweenness = mydict.get(e)

        G.remove_edge(node1, node2)

    return  edgebetweenness
    """





def getPE(G, node1, node2):

    if G.has_edge(node1, node2):
        return G[node1][node2]['twok']

    else:
        G.add_edge(node1, node2)
        nx.set_edge_attributes(G, 'dummyzerok', dict(zip(G.edges(), [0.5 for edge in G.edges()])))
        nx.set_edge_attributes(G, 'dummyonek', dict(zip(G.edges(), [-1 for edge in G.edges()])))
        nx.set_edge_attributes(G, 'dummytwok', dict(zip(G.edges(), [-1 for edge in G.edges()])))

        for u, v, data in G.edges_iter(data=True):

            commonneig = nx.common_neighbors(G, u, v)

            summ = 1
            count = 0
            for neig in commonneig:
                summ = summ * (1 - (G[u][neig]['dummyzerok'] * G[v][neig]['dummyzerok']))
                count = count + 1
                # edge1 = getEdgeConnectingNodes(G, node1, neig)
                # edge2 = getEdgeConnectingNodes(G, node2, neig)

            # Set 1-result as the onek
            if count == 0:
                G[u][v]['dummyonek'] = 0
            else:
                G[u][v]['dummyonek'] = summ

        for u, v, data in G.edges_iter(data=True):
            commonneig = nx.common_neighbors(G, u, v)

            summ = 1
            count = 0
            for neig in commonneig:
                summ = summ * (1 - (G[u][neig]['dummyonek'] * G[v][neig]['dummyonek']))
                count = count + 1
                # edge1 = getEdgeConnectingNodes(G, node1, neig)
                # edge2 = getEdgeConnectingNodes(G, node2, neig)

            # Set 1-result as the onek
            if count == 0:
                G[u][v]['dummytwok'] = 0
            else:
                G[u][v]['dummytwok'] = summ

        result = G[node1][node2]['dummytwok']
        G.remove_edge(node1, node2)
        return result














def findshortestpathmedianmode(G, node1, node2, sel):
    spairset = set()
    neighborlist1 = G.neighbors(node1)
    neighborlist2 = G.neighbors(node2)

    if node2 in neighborlist1:
        neighborlist1.remove(node2)

    if node1 in neighborlist2:
        neighborlist2.remove(node1)


    splist = list()
    #splist = []


    for nodeInAList in neighborlist1:
        for nodeInBList in neighborlist2:
            if nodeInAList == nodeInBList:
                continue
            elif (str(node1) + str(" - ") + str(node2)) in spairset:
                continue
            elif (str(node2) + str(" - ") + str(node1)) in spairset:
                continue
            else:
                sp = nx.shortest_path(G, source=node1, target=node2)
                splength = len(sp) - 1
                splist.append(splength)
                spairset.add(str(node1) + str(" - ") + str(node2))
                spairset.add(str(node2) + str(" - ") + str(node1))

    if sel == 0:
        if len(splist) == 0:
            return None
        else:
            med = median(splist)
            return med




def findRootedPR(G, node1, node2):
    timesRestarted = 0.0
    length = 0.0
    timesReached = 0.0
    reachedLengths = list()

    currentnode = node1

    while(1):
        if random.randint(1, 50) == 1:
            if timesRestarted >= 5:
                if len(reachedLengths) == 0:
                    return -1
                else:
                    return (sum(reachedLengths)/float(len(reachedLengths)))*(timesReached/float(timesRestarted))

            currentnode = node1#reset
            timesRestarted = timesRestarted + 1
            length = 0.0
        else:
            currentnode = random.choice(list(G[currentnode].keys()))
            length = length + 1

            if currentnode == node2:
                timesReached = timesReached + 1
                reachedLengths.append(length)

                if timesRestarted >= 5:
                    return (sum(reachedLengths) / float(len(reachedLengths))) * (timesReached / float(timesRestarted))

                currentnode = node1  # reset
                timesRestarted = timesRestarted + 1
                length = 0.0




def findHittingTime(G, node1, node2):

    currentnode = node1
    steps=0

    while(currentnode != node2) :
        currentnode = random.choice(list(G[currentnode].keys()))
        steps = steps + 1

    return steps



def findBoundedHittingTime(G, node1, node2, limit):
    currentnode = node1
    steps = 0

    while(currentnode != node2) :
        if(steps > limit):
            return -1
        currentnode = random.choice(list(G[currentnode].keys()))
        steps = steps + 1

    return steps


def findCommuteTime(G, node1, node2):
    return findHittingTime(G, node1, node2) + findHittingTime(G, node2, node1)


def findBoundedCommuteTime(G, node1, node2, limit):
    x = findBoundedHittingTime(G, node1, node2, limit)
    if(x == -1):
        return x
    y = findBoundedHittingTime(G, node2, node1, limit)
    if (y == -1):
        return y

    return x+y


def runPE(G):

    nx.set_edge_attributes(G, 'zerok', dict(zip(G.edges(), [0.5 for edge in G.edges()])))
    nx.set_edge_attributes(G, 'onek', dict(zip(G.edges(), [-1 for edge in G.edges()])))
    nx.set_edge_attributes(G, 'twok', dict(zip(G.edges(), [-1 for edge in G.edges()])))

    for u, v, data in G.edges_iter(data=True):

        commonneig = nx.common_neighbors(G, u, v)

        summ = 1
        count = 0
        for neig in commonneig:
            summ = summ * (1 - (G[u][neig]['zerok'] * G[v][neig]['zerok']))
            count = count + 1
            #edge1 = getEdgeConnectingNodes(G, node1, neig)
            #edge2 = getEdgeConnectingNodes(G, node2, neig)

        # Set 1-result as the onek
        if count == 0:
            G[u][v]['onek'] = 0
        else:
            G[u][v]['onek'] = summ



    for u, v, data in G.edges_iter(data=True):
        commonneig = nx.common_neighbors(G, u, v)

        summ = 1
        count = 0
        for neig in commonneig:
            summ = summ * (1 - (G[u][neig]['onek'] * G[v][neig]['onek']))
            count = count + 1
            # edge1 = getEdgeConnectingNodes(G, node1, neig)
            # edge2 = getEdgeConnectingNodes(G, node2, neig)

        # Set 1-result as the onek
        if count == 0:
            G[u][v]['twok'] = 0
        else:
            G[u][v]['twok'] = summ



def hasEdge(G, node1, node2):
    if G.has_edge(node1, node2):
        return "y"
    else:
        return "n"


def getEdgeConnectingNodes(G, node1, node2):
    node1edges = G.edges(node1)
    for edge in node1edges:
        if ((edge[0] == node1 and edge[1] == node2) or (edge[0] == node1 and edge[1] == node2)):
            return edge



def main():
    TABD = "\t"
    CSVD = ","
    newld = "\n"
    writedelim = CSVD

    parser = argparse.ArgumentParser(description='Optional app description')
    parser.add_argument('filename', type=str, help='String file name')
    parser.add_argument('delimiter', type=int, help='Delimiter to read 0-tab, 1-csv')
    parser.add_argument('rank', type=int, help='Rank while calculating SVD')
    parser.add_argument('maxSteps', type=int, help='Maximum steps for random walk')
    args = parser.parse_args()

    if args.delimiter == 0:
        readdelim = TABD
    else:
        readdelim = CSVD

    RANK = args.rank
    MAXSTEPS = args.maxSteps
    BETA = 0.005

    print("~~~ File entered is "+ args.filename)
    print("~~~ Delimiter to be used is  "+ "tabd" if readdelim == TABD else "csvd")
    print("~~~ Rank entered is "+ str(RANK))
    print("~~~ Maximum steps for random walk are " + str(MAXSTEPS) + " steps")
    print("~~~ Creating graph ....")

    G = nx.read_edgelist(args.filename, delimiter=readdelim)
    print(nx.info(G))
    print("~~~ Finding Edge Features ...")

    f = open('edgefeatures.csv','w')

    f.write("Node-Pair")
    f.write(writedelim)
    f.write("ResourceAllocationIndex")
    f.write(writedelim)
    f.write("JaccardCoefficient")
    f.write(writedelim)
    f.write("Adar")
    f.write(writedelim)
    f.write("Preferential Attachment")
    f.write(writedelim)
    f.write("CommonNeighbours")
    f.write(writedelim)
    f.write("Cosine")
    f.write(writedelim)
    f.write("Power Law Exponent")
    f.write(writedelim)
    f.write("Katz Result")
    f.write(writedelim)
    f.write("SVD Result")
    f.write(writedelim)
    f.write("Edge Betweenness")
    f.write(writedelim)
    f.write("Flow value")
    f.write(writedelim)
    f.write("Shortest Path Median")
    f.write(writedelim)
    f.write("RootedPR")
    f.write(writedelim)
    f.write("Hitting Time")
    f.write(writedelim)
    f.write("Commute Time")
    f.write(writedelim)
    f.write("Bounded Hitting Time")
    f.write(writedelim)
    f.write("Bounded Commute Time")
    f.write(writedelim)


    f.write("PE-measure")
    f.write(writedelim)

    f.write("ShortestPath")
    f.write(writedelim)

    f.write("Class")
    f.write(newld)




    # Katz start
    adj_matrix = nx.adjacency_matrix(G)
    numpy_adj_matrix = nx.to_numpy_matrix(G)
    # print G.nodes()
    # print "adj"
    # print numpy_adj_matrix

    pOneMatrix = numpy_adj_matrix
    pTwoMatrix = numpy_adj_matrix * numpy_adj_matrix
    pThreeMatrix = pTwoMatrix * pOneMatrix
    pFourMatrix = pThreeMatrix * pOneMatrix


    # Svd start
    u, s, v = np.linalg.svd(numpy_adj_matrix, full_matrices=False)
    Ar = np.zeros((len(u), len(v)))
    for i in range(RANK):
            Ar += s[i] * np.outer(u.T[i], v[i])

    # PE
    runPE(G)

    # Get components
    nodelist = G.nodes()
    components = nx.connected_components(G)
    pairset = set() # initializing a set,  {} is dictionary



    # Others start
    for nodes in components:
        for node1 in nodes:
            for node2 in nodes:
                if node1 == node2:
                    continue

                elif (str(node1)+str(" - ")+str(node2)) in pairset:
                    continue

                elif (str(node2) + str(" - ") + str(node1)) in pairset:
                    continue

                else:
                    rai = getRAIndex(G, node1, node2)
                    jc = getJaccardIndex(G, node1, node2)
                    aai = getAtomicAdarIndex(G, node1, node2)
                    pa = getPrefAtt(G, node1, node2)
                    sp = getShortestPath(G, node1, node2)
                    cnlen = getNumCommonNeighbour(G, node1, node2)
                    cosine = getCosine(G, node1, node2)
                    unionlength = getNumUnionNeighbour(G, node1, node2)
                    ple = getPowerLawExponent(G, node1, node2)


                    pOne = pOneMatrix[nodelist.index(node1), nodelist.index(node2)]
                    pTwo = pTwoMatrix[nodelist.index(node1), nodelist.index(node2)]
                    pThree = pThreeMatrix[nodelist.index(node1), nodelist.index(node2)]
                    pFour = pFourMatrix[nodelist.index(node1), nodelist.index(node2)]
                    katzresult = (BETA * pOne) +  (BETA * BETA * pTwo) + (BETA * BETA * BETA * pThree) + (BETA * BETA * BETA * BETA * pFour)

                    svdresult = Ar[nodelist.index(node1), nodelist.index(node2)]

                    flowvalue = getMaximumFlow(G, node1, node2)

                    edgebetweenness = getEdgeBetweenness(G, node1, node2)

                    shortestpathmedian = findshortestpathmedianmode(G, node1, node2, 0)

                    rootedPR = findRootedPR(G, node1, node2)


                    hittingtime = findHittingTime(G, node1, node2)
                    commutetime = findCommuteTime(G, node1, node2)
                    bouhittingtime = findBoundedHittingTime(G, node1, node2, MAXSTEPS)
                    boucommutetime = findBoundedCommuteTime(G, node1, node2, MAXSTEPS)


                    f.write(str(node1)+str(" - ")+str(node2))
                    f.write(writedelim)

                    for u, v, p in rai:
                        f.write(str(p))
                        f.write(writedelim)

                    for u, v, p in jc:
                        f.write(str(p))
                        f.write(writedelim)

                    for u, v, p in aai:
                        f.write(str(p))
                        f.write(writedelim)

                    for u, v, p in pa:
                        f.write(str(p))
                        f.write(writedelim)


                    f.write(str(cnlen))
                    f.write(writedelim)

                    f.write(str(cosine))
                    f.write(writedelim)

                    f.write(str(ple))
                    f.write(writedelim)

                    f.write(str(katzresult))
                    f.write(writedelim)

                    f.write(str(svdresult))
                    f.write(writedelim)

                    f.write(str(edgebetweenness))
                    f.write(writedelim)

                    f.write(str(flowvalue))
                    f.write(writedelim)

                    f.write(str(shortestpathmedian))
                    f.write(writedelim)

                    f.write(str(rootedPR))
                    f.write(writedelim)

                    f.write(str(hittingtime))
                    f.write(writedelim)

                    f.write(str(commutetime))
                    f.write(writedelim)

                    f.write(str(bouhittingtime))
                    f.write(writedelim)

                    f.write(str(boucommutetime))
                    f.write(writedelim)

                    f.write(str(getPE(G, node1, node2)))
                    f.write(writedelim)

                    f.write(str(len(sp) - 1))
                    f.write(writedelim)

                    f.write(hasEdge(G, node1, node2))
                    f.write(newld)

                    pairset.add(str(node1)+str(" - ")+str(node2))
                    pairset.add(str(node2) + str(" - ") + str(node1))


    f.close()
    print("~~~ Finished writing to the file named edgefeatures.csv ...")


main()
